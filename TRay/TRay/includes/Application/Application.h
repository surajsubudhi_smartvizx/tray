#pragma once

#include <Utils/EnumTypes.h>
//#include <Camera/Camera.h>

namespace TRAY 
{
	class Application 
	{
	public:
		Application(int aWidth, int aHeight, eTracerType aTracerType, unsigned int aNumOfDevices = 1, unsigned int aStackSize = 1024);
		~Application();

		void Display() const;
		void SetCamera(class Camera* aCam);

		class Window* GetWindow() const;

	private:
		
		void InitTracer() const;

	private:
		class Camera*		mCamera;
		class Window*		mWindow;
		class Tracer*		mTracer;

	};
}

