#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>
#include <iostream>
#include <memory>

namespace TRAY 
{

	// TODO: Move the static Function to some other where suitable may be in header file where all static stuff present
	static void ERROR_CALLBACK(int error, const char* description)
	{
		std::cerr << "Error!! " << error << " : " << description << "\n";
	}

	class Window 
	{
	public:
		Window(int aWidth, int aHeight, const char* aTitle);
		~Window();

		void Close();
		void Update() const;
		bool IfWindowClosed();
		void Reshape(int aNewWidth, int aNewHeight);

		int GetWidth() const;
		int GetHeight() const;

	private:
		int  Init();
		void InitGL();
		void InitGLSL();
		void CheckInfoLog(const char* msg, GLuint object);

		int					mWidth;
		int					mHeight;
		const char*			mWindowTitle;
		GLFWwindow*			mWindowPtr;

		// OpenGL variables:
		GLuint				mHdrTexture;
		// GLSL shaders objects and program.
		GLuint				mGlslVs;
		GLuint				mGlslFs;
		GLuint				mGlslProgram;

	};

	typedef std::shared_ptr<Window> WindowPtr;
}

