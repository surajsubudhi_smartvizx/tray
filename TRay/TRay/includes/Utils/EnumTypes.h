#pragma once

namespace TRAY 
{
	enum class eTracerType
	{
		OPTIX_TRACER,
		OTHER_TRACER // for Radeon Rays 
	};

} // End of namespace TRAy