#pragma once

#include "Utils/Vector3.h"

namespace TRAY 
{
	class Camera 
	{
	public:
		Camera();
		Camera(Vector3F aCamPos, Vector3F aLookAt, Vector3F aUp);
		virtual ~Camera();

		bool GetFrustum(Vector3F& pos, Vector3F& u, Vector3F& v, Vector3F& w);
		float GetAspectRatio() const;

	protected:
		int mWidth;
		int mHeight;
		float mAspectRatio;

		Vector3F mLookAt;
		Vector3F mUp;
		
		Vector3F mCameraPos;
		Vector3F mCameraU;
		Vector3F mCameraV;
		Vector3F mCameraW;

		bool bIsDirty;

	};
} // End of namespace TRAY
