#pragma once

#include <Tracer/Tracer.h>

#include <optix.h>
#include <optixu/optixpp_namespace.h>
#include <map>
#include "Utils/Constants.h"

namespace TRAY 
{
	// TODO: Move the static Function to some other file where suitable may be in header file where all static stuff present
	static std::string ptxPath(std::string const& cuda_file)
	{
		return  std::string(DIR_PATH) + std::string("/") + std::string(PROJECT_NAME) +
				std::string("_generated_") + cuda_file + std::string(".ptx");
	}

	class OptixTracer : public Tracer
	{
	public:
		OptixTracer(unsigned int aDevices, unsigned int aStackSize);

		void GetDeviceInformation() const override;
		void Render() override;
		void InitScene() override;

		void SetNumOfEntryPoint(int aNum = 1);
		void SetStackSize(unsigned int aStackSize);

	private:
		void InitOptix();
		void InitOptixPrograms();
		void SetupOptixPrograms();
		void SetDevicesToContext();
		void SetupRenderer();

	private:
		unsigned int mDeviceEncoding;
		unsigned int mStackSize;

		std::map<std::string, optix::Program> mMapOfPrograms;
		optix::Context mContext;
		optix::Buffer mBufferOutput;

	};
}
