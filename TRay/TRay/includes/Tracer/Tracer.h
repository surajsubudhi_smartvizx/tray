#pragma once

namespace TRAY 
{
	class Tracer 
	{
	public:
		Tracer();
		virtual ~Tracer();

		virtual void GetDeviceInformation() const = 0;
		virtual void Render() = 0;
		virtual void InitScene() = 0;

		void SetApplication(class Application* aApp);

	protected:
		class Application* mApplicationPtr = nullptr;
	};

}
