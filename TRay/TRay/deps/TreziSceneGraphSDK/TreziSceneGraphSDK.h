#pragma once

#ifdef TREZISCENEGRAPHSDK_EXPORTS
#define TREZISCENEGRAPHSDK_API __declspec(dllexport)
#else
#define TREZISCENEGRAPHSDK_API __declspec(dllimport)
#endif


#include <stdint.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <iomanip>
#include <map>
#include <set>

namespace TreziSceneGraph
{
    using namespace std;

    /**
    * Struct to store File details of any absolute path.
    */
    struct FileDetail
    {
        string FilePath;
        string FileName;
        string FileExtension;
        FileDetail()
        {
            FilePath = "";
            FileName = "";
            FileExtension = "";
        }
    };

    /**
    * Struct to store Geometry data of a Mesh
    */
    struct TreziMeshHeader
    {
        unsigned int NumVertices;
        float *Vertices;						// Size = NumVertices * 3 * sizeof(float)
        unsigned int NumIndices;
        unsigned int *Indices;					// Size = NumIndices * sizeof(unsigned int)
        bool IsNormalsPresent;
        float *Normals;							//Size = NumVertices * 3 * sizeof(float)
        bool IsColorValuesPresent;
        float *VertexColors;					// Size = NumVertices * 4 * sizeof(float)
        bool IsUV0Present;
        float *UV0;								// Size = NumVertices * 2 * sizeof(float)

        TreziMeshHeader()
        {
            NumVertices = 0;
            NumIndices = 0;
            IsNormalsPresent = false;
            IsColorValuesPresent = false;
            IsUV0Present = false;
            Vertices = nullptr;
            Indices = nullptr;
            Normals = nullptr;
            VertexColors = nullptr;
            UV0 = nullptr;
        }

		~TreziMeshHeader()
		{
			if (Vertices)
			{
				delete[]Vertices;
			}
			if (Normals)
			{
				delete[]Normals;
			}
			if (VertexColors)
			{
				delete[]VertexColors;
			}
			if (UV0)
			{
				delete[]UV0;
			}
			if (Indices)
			{
				delete[]Indices;
			}
		}
    };

    /**
    * Struct to store Color data
    */
    struct TreziColor4
    {
        float r;
        float g;
        float b;
        float a;
        vector<float> Get()
        {
            vector<float> Data;
            Data.push_back(r);
            Data.push_back(g);
            Data.push_back(b);
            Data.push_back(a);
            return Data;
        }
        TreziColor4()
        {
            r = 0;
            g = 0;
            b = 0;
            a = 0;
        }
		TreziColor4(float val_r, float val_g, float val_b, float val_a)
		{
			r = val_r;
			g = val_g;
			b = val_b;
			a = val_a;
		}

    };

    /**
    * Struct to store 3 dimensional vector information along with
    * the value of W
    */
    struct TreziVec4
    {
        float x;
        float y;
        float z;
        float w;
        vector<float> Get()
        {
            vector<float> Data;
            Data.push_back(x);
            Data.push_back(y);
            Data.push_back(z);
            Data.push_back(w);
            return Data;
        }
        TreziVec4()
        {
            x = 0;
            y = 0;
            z = 0;
            w = 0;
        }
		TreziVec4(float val_x, float val_y, float val_z, float val_w)
		{
			x = val_x;
			y = val_y;
			z = val_z;
			w = val_w;
		}
    };

    /**
    * Struct to store 3 dimensional vector information
    */
    struct TreziVec3
    {
        float x;
        float y;
        float z;
        vector<float> Get()
        {
            vector<float> Data;
            Data.push_back(x);
            Data.push_back(y);
            Data.push_back(z);
            return Data;
        }
        TreziVec3()
        {
            x = 0;
            y = 0;
            z = 0;
        }
		TreziVec3(float val_x, float val_y, float val_z)
		{
			x = val_x;
			y = val_y;
			z = val_z;
		}
    };

    /**
    * Struct to store 2 dimensional vector information
    */
    struct TreziVec2
    {
        float x;
        float y;
        vector<float> Get()
        {
            vector<float> Data;
            Data.push_back(x);
            Data.push_back(y);
            return Data;
        }
        TreziVec2()
        {
            x = 0;
            y = 0;
        }
		TreziVec2(float val_x, float val_y)
		{
			x = val_x;
			y = val_y;
		}
    };


	struct TreziMat16
	{
		vector<float> mat16;

		vector<float> Get()
		{

			return mat16;
		}

		TreziMat16()
		{
			for (int r = 0; r < 4; r++)
			{
				for (int c = 0; c < 4; c++)
				{
					if (r == c)
					{
						mat16.push_back(1.0f);
					}
					else
					{
						mat16.push_back(0.0f);
					}
				}
			}
		}
	};


    /**
    * Struct to store all the data required for creating a 3D geometry
    * and assigning a meterial to that mesh.
    */
    struct TreziMesh
    {
        string Name;
        string UID; // Use this for saving or creating a <UID>.bin file
        FileDetail MeshFileDetail;
        vector<TreziVec3> Vertices;
        vector<int> Indices;
        vector<TreziVec3> Normals;
        vector<TreziColor4> VertexColors;
        vector<TreziVec2> UV0;
        int MaterialIndex;

        TreziMesh()
        {
            Name = "";
            UID = ""; // Use this for saving or creating a <UID>.bin file
            Vertices.clear();
            Indices.clear();
            Normals.clear();
            VertexColors.clear();
            UV0.clear();
            MaterialIndex = -1;
        }
		~TreziMesh()
		{
			Vertices.clear();
			Indices.clear();
			Normals.clear();
			VertexColors.clear();
			UV0.clear();
		}
    };

    /**
    * Struct to store path, tiling and offset of a texture.
    */
    struct TMMap
    {
        string Path;
        TreziVec2 Tiling;
        TreziVec2 Offset;
        TMMap()
        {
            Path = "";
        }
    };

    /**
    * Struct to store Diffuse surface property
    */
    struct TMDiffuseSurfaceProp
    {
        TreziColor4 Color;
        TMMap * Map;
        TMDiffuseSurfaceProp()
        {
            Map = new TMMap();
        }
		~TMDiffuseSurfaceProp()
		{
			if (Map)
			{
				delete Map;
				Map = nullptr;
			}
		}
    };

    /**
    * Struct to store Roughness surface property
    */
    struct TMRoughnessSurfaceProp
    {
        float Value;
        TMMap * Map;
        TMRoughnessSurfaceProp()
        {
            Value = -1;
            Map = new TMMap();
        }
		~TMRoughnessSurfaceProp()
		{
			if (Map)
			{
				delete Map;
				Map = nullptr;
			}
		}
    };

    /**
    * Struct to store Metallic surface property
    */
    struct TMMetallicSurfaceProp
    {
        float Value;
        TMMap * Map;
        TMMetallicSurfaceProp()
        {
            Value = -1;
            Map = new TMMap();
        }
		~TMMetallicSurfaceProp()
		{
			if (Map)
			{
				delete Map;
				Map = nullptr;
			}
		}
    };

    /**
    * Struct to store Normal surface property
    */
    struct TMNormalSurfaceProp
    {
        TreziVec3 Value;
        TMMap * Map;
        TMNormalSurfaceProp()
        {
            Value.x = 0.25f;
            Value.y = 0.25f;
            Value.z = 0.25f;
            Map = new TMMap();
        }
		~TMNormalSurfaceProp()
		{
			if (Map)
			{
				delete Map;
				Map = nullptr;
			}
		}
    };

    /**
    * Struct to store Opacity surface property
    */
    struct TMOpacitySurfaceProp
    {
        float Value;
        TMMap * Map;
        TMOpacitySurfaceProp()
        {
            Value = -1;
            Map = new TMMap();
        }
		~TMOpacitySurfaceProp()
		{
			if (Map)
			{
				delete Map;
				Map = nullptr;
			}
		}
    };

    /**
    * Struct to store Emissive surface property
    */
    struct TMEmissiveSurfaceProp
    {
        TreziColor4 Color;
        TMMap * Map;
        TMEmissiveSurfaceProp()
        {
            Color.r = 0;
            Color.g = 0;
            Color.b = 0;
            Color.a = 1;
            Map = new TMMap();
        }
		~TMEmissiveSurfaceProp()
		{
			if (Map)
			{
				delete Map;
				Map = nullptr;
			}
		}
    };

    /**
    * Struct to store Render property of a material
    */
    struct TMRenderProp
    {
        string Mode;
        bool IsDoubleSided;
        bool IsSpecularHighlight;
        bool IsReflective;

        TMRenderProp()
        {
            Mode = "";
            IsDoubleSided = false;
            IsSpecularHighlight = false;
            IsReflective = false;
        }
    };

    /**
    * Struct to store Surface property of a material
    */
    struct TMSurfaceProp
    {
        TMDiffuseSurfaceProp * DiffuseProp;
        TMRoughnessSurfaceProp * RoughnessProp;
        TMMetallicSurfaceProp * MetallicProp;
        TMNormalSurfaceProp * NormalProp;
        TMOpacitySurfaceProp * OpacityProp;
        TMEmissiveSurfaceProp * EmissiveProp;

        TMSurfaceProp()
        {
            DiffuseProp = new TMDiffuseSurfaceProp();
            RoughnessProp = new TMRoughnessSurfaceProp();
            MetallicProp = new TMMetallicSurfaceProp();
            NormalProp = new TMNormalSurfaceProp();
            OpacityProp = new TMOpacitySurfaceProp();
            EmissiveProp = new TMEmissiveSurfaceProp();
        }
		~TMSurfaceProp()
		{
			if (DiffuseProp)
			{
				delete DiffuseProp;
				DiffuseProp = nullptr;
			}
			if (RoughnessProp)
			{
				delete RoughnessProp;
				RoughnessProp = nullptr;
			}
			if (MetallicProp)
			{
				delete MetallicProp;
				MetallicProp = nullptr;
			}
			if (NormalProp)
			{
				delete NormalProp;
				NormalProp = nullptr;
			}
			if (OpacityProp)
			{
				delete OpacityProp;
				OpacityProp = nullptr;
			}
			if (EmissiveProp)
			{
				delete EmissiveProp;
				EmissiveProp = nullptr;
			}
		}
    };

    /**
    * Struct to store material information of a single mesh
    */
    struct TreziMaterial
    {
        string Name;
        bool IsTranslucent;
        float OpacityValue;
        string URI;
        vector<string> DiffuseTextures;
        vector<string> SpecularTextures;
        vector<string> AmbientTextures;
        vector<string> EmissiveTextures;
        vector<string> NormalTextures;
        vector<string> OpacityTextures;
        TreziColor4 AmbientColor;
        TreziColor4 DiffuseColor;
        TreziColor4 EmissiveColor;
        TreziColor4 SpecularColor;
        TreziColor4 TransparentColor;
        TMRenderProp * RenderProperties;
        TMSurfaceProp * SurfaceProperties;

        TreziMaterial()
        {
            Name = "";
            IsTranslucent = false;
            OpacityValue = 1.0f;
            URI = "";
            DiffuseTextures.clear();
            SpecularTextures.clear();
            AmbientTextures.clear();
            EmissiveTextures.clear();
            NormalTextures.clear();
            OpacityTextures.clear();
            RenderProperties = new TMRenderProp();
            SurfaceProperties = new TMSurfaceProp();
        }

		~TreziMaterial()
		{
			DiffuseTextures.clear();
			SpecularTextures.clear();
			AmbientTextures.clear();
			EmissiveTextures.clear();
			NormalTextures.clear();

			if (RenderProperties)
			{
				delete RenderProperties;
				RenderProperties = nullptr;
			}

			if (SurfaceProperties)
			{
				delete SurfaceProperties;
				SurfaceProperties = nullptr;
			}
		}
    };

    /**
    * Struct to store Node data of a Scene Graph
    */
    struct TreziNode
    {
        string Name;
        string UID;
        bool IsActive;
        vector<TreziNode *> Children;
        vector<int> MeshIndices;
        TreziVec3 Scale;
        TreziVec4 Rotation;
        TreziVec3 Translation;

		TreziNode()
		{
			Children.clear();
			MeshIndices.clear();
		}

		~TreziNode()
		{
			for (vector<TreziNode*>::iterator it = Children.begin(); it < Children.end(); it++)
			{
				if (*it)
				{
					delete *it;
					*it = nullptr;
				}
			}
			//Children.clear();

			MeshIndices.clear();
		}

		TreziMat16* TransformMat;
    };

    /**
    * Struct to store Scene Graph data of one scene.
    * It could have [0, INFINITE) number of nodes and
    * all other scene data
    */
    struct TreziScene
    {
        string Name;
        string UID;
        FileDetail SceneFileDetail;
        TreziNode * RootNode;
        vector<TreziMesh *> Meshes;
        vector<TreziMaterial *> Materials;

		TreziScene()
		{
			RootNode = nullptr;
			Meshes.clear();
			Materials.clear();
		}

		~TreziScene()
		{
			if (RootNode)
			{
				delete RootNode;
				RootNode = nullptr;
			}

			for (vector<TreziMesh*>::iterator it = Meshes.begin(); it < Meshes.end(); it++)
			{
				if (*it)
				{
					delete *it;
					*it = nullptr;
				}
			}

			for (vector<TreziMaterial*>::iterator it = Materials.begin(); it < Materials.end(); it++)
			{
				if (*it)
				{
					delete *it;
					*it = nullptr;
				}
			}
			//Meshes.clear();
			//Materials.clear();
		}
    };

    /*** ERROR CODES ***/
    const int TREZI_SUCCESS = 1;
    const int TREZI_ERROR_FILE_NOT_FOUND = 0;
    const int TREZI_ERROR_INVALID_VERTICES = 2;
    const int TREZI_ERROR_INVALID_INDICES = 3;
    const int TREZI_ERROR_UNKNOWN = 4;

#define NUM_VERTICES_SIZE (sizeof(unsigned int))
#define NUM_INDICES_SIZE (sizeof(unsigned int))
#define INDEX_SIZE (sizeof(unsigned int))
#define VERTEX_SIZE (sizeof(float) * 3)
#define UV_SIZE (sizeof(float) * 2)
#define COLOR_SIZE (sizeof(float) * 4)
#define NORMAL_SIZE (sizeof(float) * 3)

    /**
    * Get the Trezi Scene Graph from the specified input file (.json)
    * @param InputFileName Absolute file path for the input file
    * @return An instance of TreziScene if the input file has a valid scene, nullptr otherwise
    */
    extern "C" TREZISCENEGRAPHSDK_API TreziScene * GetScene(const char * InputFileName);

    /**
    * Save the Trezi Scene into a .json. The output filename (.json) is
    * constructed using the FileDetail data from the TreziScene instance.
    * @param CurrentScene The pointer to the TreziScene instance which contains the Scene data
    * @return true if the Scene is saved successfully into a .json file, false otherwise
    */
    extern "C" TREZISCENEGRAPHSDK_API bool SaveScene(TreziScene * CurrentScene);

    /**
    * Create a new instance of TreziScene with default values.
    * @param Name the name to be used while creating the new instance
    * @param UID The UID to be assigned to the new instance.
    * @return Always return a new instance of TreziScene
    */
    extern "C" TREZISCENEGRAPHSDK_API TreziScene * CreateNewTreziScene(const char * Name, const char * UID = '\0');

    /**
    * Create a new instance of TreziNode with default values.
    * @param Name The name to be used while creating the new instance
    * @param UID The UID to be assigned to the new instance.
    * @return Always return a new instance of TreziNode
    */
    extern "C" TREZISCENEGRAPHSDK_API TreziNode * CreateNewTreziNode(const char * Name, const char * UID = '\0');

    /**
    * Create a new instance of CreateNewTreziMaterial with default values.
    * @param Name The name to be used while creating the new instance
    * @return Always return a new instance of CreateNewTreziMaterial
    */
    extern "C" TREZISCENEGRAPHSDK_API TreziMaterial * CreateNewTreziMaterial(const char * Name);

    /**
    * Create a new instance of TreziMesh with default values.
    * @param Name The name to be used while creating the new instance
    * @param UID The UID to be assigned to the new instance.
    * @return Always return a new instance of TreziMesh
    */
    extern "C" TREZISCENEGRAPHSDK_API TreziMesh * CreateNewTreziMesh(const char * Name, const char * UID = '\0');


	/**
	* Create a new instance of TreziVec2 with default values.
	* @param x The X co-ordinate value
	* @param y The Y co-ordinate value
	* @return Always return a new instance of TreziVec2
	*/
	extern "C" TREZISCENEGRAPHSDK_API TreziMat16 * CreateNewTreziMat16();


    /**
    * Check if a file exists on the system or not
    * @param FileName Absolute file path for the input file to be checked
    * @return true if the file exists, false otherwise
    */
    extern "C" TREZISCENEGRAPHSDK_API bool DoesFileExist(const char * FileName);
}
