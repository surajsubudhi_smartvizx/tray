#include <Application/Application.h>
#include <Application/Window.h>
#include <Tracer/OptixTracer.h>

namespace TRAY 
{
	Application::Application(int aWidth, int aHeight, eTracerType aTracerType, unsigned int aNumOfDevices, unsigned int aStackSize)
	{
		mWindow = new Window(aWidth, aHeight, "Trezi Raytracer.");
		
		if (aTracerType == eTracerType::OPTIX_TRACER) 
		{
			mTracer = new OptixTracer(aNumOfDevices, aStackSize);
		}
	}

	Application::~Application()
	{
		if (mWindow != nullptr) 
		{
			delete mWindow;
			mWindow = nullptr;
		}

		if (mTracer != nullptr) 
		{
			delete mTracer;
			mTracer = nullptr;
		}
	}

	void Application::Display() const
	{


	}

	void Application::SetCamera(Camera * aCam)
	{
		mCamera = aCam;
	}

	
	Window* Application::GetWindow() const
	{
		return mWindow;
	}

	void Application::InitTracer() const
	{
	}


}
