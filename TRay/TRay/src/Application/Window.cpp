#include <Application/Window.h>

namespace TRAY 
{
	void framebuffer_size_callback(GLFWwindow* aWindow, int aWidth, int aHeight);

	Window::Window(int aWidth, int aHeight, const char * aTitle) :
		mWidth(aWidth), mHeight(aHeight), mWindowTitle(aTitle)
	{
		mGlslVs      = 0;
		mGlslFs      = 0;
		mGlslProgram = 0;
		Init();
	}

	Window::~Window()
	{
		glfwTerminate();
		mWindowPtr = nullptr;
	}

	void Window::Close()
	{
		glfwSetWindowShouldClose(mWindowPtr, true);
	}

	void Window::Update() const
	{
		glfwSwapBuffers(mWindowPtr);
		glfwPollEvents();
	}

	bool Window::IfWindowClosed()
	{
		return glfwWindowShouldClose(mWindowPtr);
	}

	void Window::Reshape(int aNewWidth, int aNewHeight)
	{
		mWidth = aNewWidth;
		mHeight = aNewHeight;
		glfwSetFramebufferSizeCallback(mWindowPtr, framebuffer_size_callback);
	}

	int Window::GetWidth() const
	{
		return mWidth;
	}

	int Window::GetHeight() const
	{
		return mHeight;
	}

	int Window::Init()
	{
		glfwSetErrorCallback(ERROR_CALLBACK);

		if (!glfwInit())
		{
			ERROR_CALLBACK(1, "GLFW failed to initialize.");
			return 1;
		}

		mWindowPtr = glfwCreateWindow(mWidth, mHeight, mWindowTitle, nullptr, nullptr);
		if (!mWindowPtr)
		{
			ERROR_CALLBACK(2, "glfwCreateWindow() failed.");
			glfwTerminate();
			return 2;
		}

		glfwMakeContextCurrent(mWindowPtr);
		glfwSwapInterval(1);
		glfwSetFramebufferSizeCallback(mWindowPtr, framebuffer_size_callback);

		// Initializing OpenGL
		glewExperimental = GL_TRUE;
		GLenum err = glewInit();
		if (GLEW_OK != err)
		{
			ERROR_CALLBACK(3, "Failed to Initialize OpenGL.");
			return 3;
		}
	}

	void Window::InitGL()
	{
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		glViewport(0, 0, mWidth, mHeight);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();


		glGenTextures(1, &mHdrTexture);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mHdrTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D, 0);

		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

		InitGLSL();
	}

	void Window::InitGLSL()
	{

		// TODO : Try reading the shader program from external file
		static const std::string vsSource =
			"#version 330\n"
			"layout(location = 0) in vec4 attrPosition;\n"
			"layout(location = 8) in vec2 attrTexCoord0;\n"
			"out vec2 varTexCoord0;\n"
			"void main()\n"
			"{\n"
			"  gl_Position  = attrPosition;\n"
			"  varTexCoord0 = attrTexCoord0;\n"
			"}\n";

		static const std::string fsSource =
			"#version 330\n"
			"uniform sampler2D samplerHDR;\n"
			"in vec2 varTexCoord0;\n"
			"layout(location = 0, index = 0) out vec4 outColor;\n"
			"void main()\n"
			"{\n"
			"  outColor = texture(samplerHDR, varTexCoord0);\n"
			"}\n";

		GLint vsCompiled = 0;
		GLint fsCompiled = 0;

		mGlslVs = glCreateShader(GL_VERTEX_SHADER);
		if (mGlslVs)
		{
			GLsizei len = (GLsizei)vsSource.size();
			const GLchar *vs = vsSource.c_str();
			glShaderSource(mGlslVs, 1, &vs, &len);
			glCompileShader(mGlslVs);
			CheckInfoLog(vs, mGlslVs);

			glGetShaderiv(mGlslVs, GL_COMPILE_STATUS, &vsCompiled);
		}

		mGlslFs = glCreateShader(GL_FRAGMENT_SHADER);
		if (mGlslFs)
		{
			GLsizei len = (GLsizei)fsSource.size();
			const GLchar *fs = fsSource.c_str();
			glShaderSource(mGlslFs, 1, &fs, &len);
			glCompileShader(mGlslFs);
			CheckInfoLog(fs, mGlslFs);

			glGetShaderiv(mGlslFs, GL_COMPILE_STATUS, &fsCompiled);
		}

		mGlslProgram = glCreateProgram();
		if (mGlslProgram)
		{
			GLint programLinked = 0;

			if (mGlslVs && vsCompiled)
			{
				glAttachShader(mGlslProgram, mGlslVs);
			}
			if (mGlslFs && fsCompiled)
			{
				glAttachShader(mGlslProgram, mGlslFs);
			}

			glLinkProgram(mGlslProgram);
			CheckInfoLog("m_glslProgram", mGlslProgram);

			glGetProgramiv(mGlslProgram, GL_LINK_STATUS, &programLinked);

			if (programLinked)
			{
				glUseProgram(mGlslProgram);
				glUniform1i(glGetUniformLocation(mGlslProgram, "samplerHDR"), 0); // texture image unit 0
				glUseProgram(0);
			}
		}

	}

	void Window::CheckInfoLog(const char * msg, GLuint object)
	{
		GLint maxLength;
		GLint length;
		GLchar *infoLog;

		if (glIsProgram(object))
		{
			glGetProgramiv(object, GL_INFO_LOG_LENGTH, &maxLength);
		}
		else
		{
			glGetShaderiv(object, GL_INFO_LOG_LENGTH, &maxLength);
		}
		if (maxLength > 1)
		{
			infoLog = (GLchar *)malloc(maxLength);
			if (infoLog != NULL)
			{
				if (glIsShader(object))
				{
					glGetShaderInfoLog(object, maxLength, &length, infoLog);
				}
				else
				{
					glGetProgramInfoLog(object, maxLength, &length, infoLog);
				}
				std::cout << infoLog << std::endl;
				free(infoLog);
			}
		}
	}

	// glfw: whenever the window size changed (by OS or user resize) this callback function executes
	// ---------------------------------------------------------------------------------------------
	void framebuffer_size_callback(GLFWwindow* aWindow, int aWidth, int aHeight)
	{
		// make sure the viewport matches the new window dimensions; note that width and
		// height will be significantly larger than specified on retina displays.
		glViewport(0, 0, aWidth, aHeight);
	}

} // End of namespace TRAY