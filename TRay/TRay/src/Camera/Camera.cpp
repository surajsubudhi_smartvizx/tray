#include <Camera/Camera.h>

namespace TRAY 
{
	Camera::Camera() :
		mWidth(1), mHeight(1), mAspectRatio(1), bIsDirty(false) // TODO : For Demo purpose I Kept the width and height as 1 (Need to fix)
	{
		mCameraPos = Vector3F(0.0f, 0.0f,  1.0f);
		mCameraU   = Vector3F(1.0f, 0.0f,  0.0f);
		mCameraV   = Vector3F(0.0f, 1.0f,  0.0f);
		mCameraW   = Vector3F(0.0f, 0.0f, -1.0f);

		mLookAt = Vector3F(0.0f);
		mUp = mCameraV;
	}

	Camera::Camera(Vector3F aCamPos, Vector3F aLookAt, Vector3F aUp) :
		mCameraPos(aCamPos), mLookAt(aLookAt), mUp(aUp),
		mWidth(1), mHeight(1), mAspectRatio(1), bIsDirty(false)	// TODO : For Demo purpose I Kept the width and height as 1 (Need to fix)
	{
		mCameraV = mUp;
		mCameraW = (mLookAt - mCameraPos).norm();
		mCameraU = (Cross(mCameraV, mCameraW)).norm();
	}

	Camera::~Camera()
	{}

	bool Camera::GetFrustum(Vector3F & pos, Vector3F & u, Vector3F & v, Vector3F & w)
	{
		if (bIsDirty) 
		{
			mCameraV = mUp;
			mCameraW = (mLookAt - mCameraPos).norm();
			mCameraU = (Cross(mCameraV, mCameraW)).norm();
			bIsDirty = false;
		}

		pos = mCameraPos;
		u = mCameraU;
		v = mCameraV;
		w = mCameraW;

		return bIsDirty;
	}

	float Camera::GetAspectRatio() const
	{
		return mAspectRatio;
	}


} // End of namespace TRAY
