#include <Tracer/OptixTracer.h>
#include <iostream>
#include <Application/Application.h>
#include <Application/Window.h>

// For rtDevice*() function error checking. No OptiX context present at that time.
#define RT_CHECK_ERROR_NO_CONTEXT( func ) \
  do { \
    RTresult code = func; \
    if (code != RT_SUCCESS) \
      std::cerr << "ERROR: Function " << #func << std::endl; \
  } while (0)

namespace TRAY 
{
	OptixTracer::OptixTracer(unsigned int aDevices, unsigned int aStackSize) :
		mDeviceEncoding(aDevices), mStackSize(aStackSize)
	{
		OptixTracer::GetDeviceInformation();
		InitOptix();
		InitOptixPrograms();
		SetStackSize(aStackSize);
		SetupOptixPrograms();
		
		SetupRenderer();
	}

	
	void OptixTracer::Render()
	{
	}

	void OptixTracer::InitScene()
	{
	}

	void OptixTracer::SetNumOfEntryPoint(int aNum)
	{
		try
		{
			mContext->setEntryPointCount(aNum);
		}
		catch(optix::Exception e)
		{
			std::cerr << e.getErrorString() << "\n";
		}
	}

	void OptixTracer::SetStackSize(unsigned int aStackSize)
	{
		if (mStackSize != aStackSize) 
		{
			mStackSize = aStackSize;
		}

		try 
		{
			mContext->setStackSize(mStackSize);
		}
		catch (optix::Exception e) 
		{
			std::cerr << e.getErrorString() << "\n";
		}

	}

	void OptixTracer::InitOptix()
	{
		try
		{
			// Creating the context
			mContext = optix::Context::create();

			// Setup Devices
			SetDevicesToContext();

			// Set number of entry point for program
			SetNumOfEntryPoint();

		}
		catch(optix::Exception e)
		{
			std::cerr << e.getErrorString() << "\n";
		}
	}

	void OptixTracer::InitOptixPrograms()
	{
		try
		{
			// First load all programs and put them into a map.
			// Programs which are reused multiple times can be queried from that map.

			// Renderer
			mMapOfPrograms["raygeneration"] = mContext->createProgramFromPTXFile(ptxPath("raygeneration.cu"), "raygeneration"); // entry point 0
			mMapOfPrograms["exception"]     = mContext->createProgramFromPTXFile(ptxPath("exception.cu"), "exception");         // entry point 0
			mMapOfPrograms["miss"]			= mContext->createProgramFromPTXFile(ptxPath("miss.cu"), "miss_gradient");			// ray type 0

			// Geometry
			// All the grometry are made of triangles so we can use same bounding box and intersection program for all the geometries.
			mMapOfPrograms["boundingbox_triangle"]	= mContext->createProgramFromPTXFile(ptxPath("boundingbox_triangle.cu"), "boundingbox_triangle");
			mMapOfPrograms["intersection_triangle"] = mContext->createProgramFromPTXFile(ptxPath("intersection_triangle.cu"), "intersection_triangle");

			// Material programs.
			// For the radiance ray type 0:
			mMapOfPrograms["closesthit"] = mContext->createProgramFromPTXFile(ptxPath("closesthit.cu"), "closesthit");
			// For the shadow or secondary rays type 1:
			mMapOfPrograms["anyhit"]	 = mContext->createProgramFromPTXFile(ptxPath("anyhit.cu"), "anythit");
		}
		catch(optix::Exception& e)
		{
			std::cerr << e.getErrorString() << "\n";
		}
	}

	void OptixTracer::SetupOptixPrograms()
	{
		try 
		{
			// TODO: Add the option for more ray type count for demo I am using only one ray.
			mContext->setRayTypeCount(1);

			std::map<std::string, optix::Program>::const_iterator it = mMapOfPrograms.find("raygeneration");
			mContext->setRayGenerationProgram(0, it->second); // entrypoint

			it = mMapOfPrograms.find("exception");
			mContext->setExceptionProgram(0, it->second); // entrypoint

			it = mMapOfPrograms.find("miss");
			mContext->setMissProgram(0, it->second); // raytype
		}
		catch(optix::Exception& e)
		{
			std::cerr << e.getErrorString() << "\n";
		}
	}

	void OptixTracer::SetDevicesToContext()
	{
		try
		{
			// Select the GPUs to use with this context.
			unsigned int numberOfDevices = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetDeviceCount(&numberOfDevices));
			std::cout << "Number of Devices = " << numberOfDevices << "\n" << "\n";

			std::vector<int> devices;

			int devicesEncoding = mDeviceEncoding; // Preserve this information, it can be stored in the system file.
			unsigned int i = 0;
			do 
			{
				int device = devicesEncoding % 10;
				devices.push_back(device); // DAR FIXME Should be a std::set to prevent duplicate device IDs in m_devicesEncoding.
				devicesEncoding /= 10;
				++i;
			} while (i < numberOfDevices && devicesEncoding);

			mContext->setDevices(devices.begin(), devices.end());

			// Print out the current configuration to make sure what's currently running.
			devices = mContext->getEnabledDevices();
			for (auto device : devices)
			{
				std::cout << "m_context is using local device " << device << ": " << mContext->getDeviceName(device) << "\n";
			}
		}
		catch(optix::Exception& e)
		{
			std::cerr << e.getErrorString() << "\n";
		}
	}

	void OptixTracer::SetupRenderer()
	{
		try 
		{
			mBufferOutput = mContext->createBuffer(RT_BUFFER_INPUT_OUTPUT);
			mBufferOutput->setFormat(RT_FORMAT_FLOAT4); // RGBA32F

			auto window = mApplicationPtr->GetWindow();
			if (window != nullptr)
			{
				mBufferOutput->setSize(window->GetWidth(), window->GetHeight());
			}

			mContext[S_OUTPUT_BUFFER]->set(mBufferOutput);


		}
		catch(optix::Exception& e)
		{
			std::cerr << e.getErrorString() << "\n";
		}

	}

	void OptixTracer::GetDeviceInformation() const
	{
		unsigned int optixVersion;
		RT_CHECK_ERROR_NO_CONTEXT(rtGetVersion(&optixVersion));

		unsigned int major = optixVersion / 1000; // Check major with old formula.
		unsigned int minor;
		unsigned int micro;
		if (3 < major) // New encoding since OptiX 4.0.0 to get two digits micro numbers?
		{
			major = optixVersion / 10000;
			minor = (optixVersion % 10000) / 100;
			micro = optixVersion % 100;
		}
		else // Old encoding with only one digit for the micro number.
		{
			minor = (optixVersion % 1000) / 10;
			micro = optixVersion % 10;
		}
		std::cout << "OptiX " << major << "." << minor << "." << micro << "\n";

		unsigned int numberOfDevices = 0;
		RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetDeviceCount(&numberOfDevices));
		std::cout << "Number of Devices = " << numberOfDevices << "\n" << "\n";

		for (unsigned int i = 0; i < numberOfDevices; ++i)
		{
			char name[256];
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_NAME, sizeof(name), name));
			std::cout << "Device " << i << ": " << name << "\n";

			int computeCapability[2] = { 0, 0 };
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY, sizeof(computeCapability), &computeCapability));
			std::cout << "  Compute Support: " << computeCapability[0] << "." << computeCapability[1] << "\n";

			RTsize totalMemory = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_TOTAL_MEMORY, sizeof(totalMemory), &totalMemory));
			std::cout << "  Total Memory: " << (unsigned long long) totalMemory << "\n";

			int clockRate = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_CLOCK_RATE, sizeof(clockRate), &clockRate));
			std::cout << "  Clock Rate: " << clockRate << " kHz" << "\n";

			int maxThreadsPerBlock = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, sizeof(maxThreadsPerBlock), &maxThreadsPerBlock));
			std::cout << "  Max. Threads per Block: " << maxThreadsPerBlock << "\n";

			int smCount = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT, sizeof(smCount), &smCount));
			std::cout << "  Streaming Multiprocessor Count: " << smCount << "\n";

			int executionTimeoutEnabled = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_EXECUTION_TIMEOUT_ENABLED, sizeof(executionTimeoutEnabled), &executionTimeoutEnabled));
			std::cout << "  Execution Timeout Enabled: " << executionTimeoutEnabled << "\n";

			int maxHardwareTextureCount = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_MAX_HARDWARE_TEXTURE_COUNT, sizeof(maxHardwareTextureCount), &maxHardwareTextureCount));
			std::cout << "  Max. Hardware Texture Count: " << maxHardwareTextureCount << "\n";

			int tccDriver = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_TCC_DRIVER, sizeof(tccDriver), &tccDriver));
			std::cout << "  TCC Driver enabled: " << tccDriver << "\n";

			int cudaDeviceOrdinal = 0;
			RT_CHECK_ERROR_NO_CONTEXT(rtDeviceGetAttribute(i, RT_DEVICE_ATTRIBUTE_CUDA_DEVICE_ORDINAL, sizeof(cudaDeviceOrdinal), &cudaDeviceOrdinal));
			std::cout << "  CUDA Device Ordinal: " << cudaDeviceOrdinal << "\n" << "\n";
		}
	}

}
